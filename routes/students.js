var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
/*
 * GET studentlist.
 */
router.get('/studentlist', function(req, res) {
  var db = req.db;
  db.collection('studentcollection').find().toArray(function (err, items) {
      res.json(items);
  });
});

router.get('/studentlistlasted', function(req, res) {
    var db = req.db;
    db.collection('studentcollection').find().sort({_id:-1}).limit(5).toArray(function (err, items) {
        res.json(items);
    });
  });
/*
 * POST to addstudent.
 */
router.post('/addstudent', function(req, res) {
  var db = req.db;
  db.collection('studentcollection').insert(req.body, function(err, result){
      res.send(
          (err === null) ? { msg: '' } : { msg: err }
      );
  });
});
/*
 * POST to updatestudent.
 */
router.put('/updatestudent/:id', function(req, res) {
    var db = req.db;
    var studentToUpdate=req.params.id;
    db.collection('studentcollection').update({"_id": mongoose.Types.ObjectId(studentToUpdate)},req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});
/*
 * DELETE to deletestudent.
 */
router.delete('/deletestudent/:id', function(req, res) {
  var db = req.db;
  var studentToDelete = req.params.id;
  db.collection('studentcollection').deleteOne({"_id": mongoose.Types.ObjectId(studentToDelete)}, function(err, result) {
    res.send(
        (err === null) ? { msg: '' } : { msg: err }
    );
  })
});
module.exports = router;
