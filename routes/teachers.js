var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
/*
 * GET studentlist.
 */
router.get('/teacherlist', function(req, res) {
  var db = req.db;
  db.collection('teachercollection').find().toArray(function (err, items) {
      res.json(items);
  });
});


router.get('/teacherlistlasted', function(req, res) {
    var db = req.db;
    db.collection('teachercollection').find().sort({_id:-1}).limit(5).toArray(function (err, items) {
        res.json(items);
    });
  });
/*
 * POST to addstudent.
 */
router.post('/addteacher', function(req, res) {
  var db = req.db;
  db.collection('teachercollection').insert(req.body, function(err, result){
      res.send(
          (err === null) ? { msg: '' } : { msg: err }
      );
  });
});
/*
 * PUT to updatestudent.
 */
router.put('/updateteacher/:id', function(req, res) {
    var db = req.db;
    var teacherToUpdate=req.params.id;
    //console.log(req.body);
    // try {
        db.collection('teachercollection').update({"_id": mongoose.Types.ObjectId(teacherToUpdate)},req.body, function(err, result){
            // console.log(1231231231);
            // console.log(result);
            // console.log(result.modifiedCount);
            res.send(
                (err === null) ? { msg: '' } : { msg: result }
            );
        });
    // } catch (error) {
    //     console.log('asdasd',error);
    // }
    
});
/*
 * DELETE to deletestudent.
 */
router.delete('/deleteteacher/:id', function(req, res) {
  var db = req.db;
  var teacherToDelete = req.params.id;
  db.collection('teachercollection').deleteOne({"_id": mongoose.Types.ObjectId(teacherToDelete)}, function(err, result) {
    //   res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
    res.send(
        (err === null) ? { msg: '' } : { msg: err }
    );
  })
});
module.exports = router;
