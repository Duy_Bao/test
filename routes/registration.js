var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

router.get('/registrationlist', function(req, res) {
  var db = req.db;
  db.collection('classcollection').find().toArray(function (err, items) {
      res.json(items);
  });
});

router.post('/addclass', function(req, res) {
  var db = req.db;
  db.collection('classcollection').insert(req.body, function(err, result){
      res.send(
          (err === null) ? { msg: '' } : { msg: err }
      );
  });
});

router.delete('/removeclass/:id', function(req, res) {
  var db = req.db;
  var classToRemove=req.params.id;
  //db.collection('classcollection').update({"_id": mongoose.Types.ObjectId(classToUpDate)},{ $pull:{"st_list":{"st_id":studentToRemove} } }, function(err, result) {
  db.collection('classcollection').remove({"_id": mongoose.Types.ObjectId(classToRemove)}, function(err, result) {
    res.send(
        (err === null) ? { msg: '' } : { msg: err }
    );
  })
});


router.get('/registrationlist/:id', function(req, res) {
  var db = req.db;
  var classId = req.params.id;
  db.collection('stlistcollection').find({ "cl_id": classId }).toArray(function (err, items) {
      res.json(items);
      //console.log(items);
  });
});
router.post('/addstudenttoclass', function(req, res) {
  var db = req.db;
  db.collection('stlistcollection').insert(req.body, function(err, result){
      res.send(
          (err === null) ? { msg: '' } : { msg: err }
      );
  });
});

router.delete('/removestudentfromclass/:id', function(req, res) {
  var db = req.db;
  var studentToRemove=req.params.id;
  //db.collection('classcollection').update({"_id": mongoose.Types.ObjectId(classToUpDate)},{ $pull:{"st_list":{"st_id":studentToRemove} } }, function(err, result) {
  db.collection('stlistcollection').remove({"_id": mongoose.Types.ObjectId(studentToRemove)}, function(err, result) {
    res.send(
        (err === null) ? { msg: '' } : { msg: err }
    );
  })
});
module.exports = router;
