var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Dashboard' });
});
router.get('/teacher', function(req, res, next) {
  res.render('teacher', { title: 'Teacher' });
});
router.get('/student', function(req, res, next) {
  res.render('student', { title: 'Student' });
});
router.get('/registration', function(req, res, next) {
  res.render('registration', { title: 'Register' });
});
router.get('/timetable', function(req, res, next) {
  res.render('timetable', { title: 'Timetable' });
});
// router.index = function(req, res){
//   res.render('index', {title: 'Page Title'});
// }

module.exports = router;
