var teacherListData = [];
var teacherListLastedData = [];
var studentListData = [];
var studentListLastedData = [];
var classListData = [];
var currentClassID;
var studentId;
var teacherId;
var classId;
// DOM Ready =============================================================
$(document).ready(function() {
    initTableLastedTeacher();
    initTableLastedStudent();
    initTableTeacher();
    initTableStudent();
    initTableRegistration();
    putClassIntoCalendar();

    $('#teacherList table tbody').on('click', 'td a.linkshowteacher', showTeacherInfo);
    $('#btnAddTeacher').on('click', addTeacher);
    $('#btnUpdateTeacher').on('click', updateTeacher);
    $('#teacherList table tbody').on('click', 'td a.linkdeleteteacher', deleteTeacher);

    $('#studentList table tbody').on('click', 'td a.linkshowstudent', showStudentInfo);
    $('#btnAddStudent').on('click', addStudent);
    $('#btnUpdateStudent').on('click', updateStudent);
    $('#studentList table tbody').on('click', 'td a.linkdeletestudent', deleteStudent);

    $('#registrationList table tbody').on('click', 'td a.linkshowdetail', initTableStudentList);
    $('#btnAddClass').on('click', addClass);
    $('#registrationList table tbody').on('click', 'td a.linkremoveclass', removeClass);
    $('#btnAddStudentToClass').on('click', addStudentToClass);
    $('#classList table tbody').on('click', 'td a.linkremovestudent', removeStudentFromClass);

    $('#caleandar').on('click', 'a.showclassdetail', showClassDetail);

});
// Functions =============================================================
function initTableLastedTeacher() {
    var tableContent = '';
    $.getJSON( '/teachers/teacherlistlasted', function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.tc_id + '</td>';
            tableContent += '<td>' + this.fullname +'</td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td>' + this.major + '</td>';
            tableContent += '</tr>';
            teacherListLastedData = data;                                   
        });
        $('#teacherListLasted table tbody').html(tableContent);
    });
};
function initTableLastedStudent() {
    var tableContent = '';
    $.getJSON( '/students/studentlistlasted', function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.st_id + '</td>';
            tableContent += '<td>' + this.fullname + '</td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '</tr>';
            studentListLastedData = data;                                   
        });
        $('#studentListLasted table tbody').html(tableContent);
    });
};
function initTableTeacher() {
    var tableContent = '';
    $.getJSON( '/teachers/teacherlist', function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.tc_id + '</td>';
            tableContent += '<td><a href="#" class="linkshowteacher" rel="' + this.fullname + '" title="Show Details">' + this.fullname + '</a></td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td>' + this.major + '</td>';
            tableContent += '<td><a class="btn btn-danger linkdeleteteacher" rel="' + this._id + '">Delete</a></td>';
            tableContent += '</tr>';
            teacherListData = data;                                   
        });
        $('#teacherList table tbody').html(tableContent);
    });
};
function showTeacherInfo(event) {
    event.preventDefault();
    var thisTeacherName = $(this).attr('rel');
    var arrayPosition = teacherListData.map(function(arrayItem) { return arrayItem.fullname; }).indexOf(thisTeacherName);
    var thisTeacherObject = teacherListData[arrayPosition];
    $('#teacherInfoName').text(thisTeacherObject.fullname);
    $('#teacherInfoAge').text(thisTeacherObject.age);
    $('#teacherInfoGender').text(thisTeacherObject.gender);
    $('#teacherInfoLocation').text(thisTeacherObject.address);
    $('#teacherInfoMajor').text(thisTeacherObject.major);

    teacherId = thisTeacherObject._id;

    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherID').val(thisTeacherObject.tc_id);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherFullName').val(thisTeacherObject.fullname);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherAge').val(thisTeacherObject.age);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherAddress').val(thisTeacherObject.address);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherGender').val(thisTeacherObject.gender);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherEmail').val(thisTeacherObject.email);
    $('#updateTeacher fieldset .form-group input#inputUpdateTeacherMajor').val(thisTeacherObject.major);
};
function addTeacher(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#addTeacher input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });

    if(errorCount === 0) {
        var newTeacher = {
            'tc_id': $('#addTeacher fieldset .form-group input#inputTeacherID').val(),
            'fullname': $('#addTeacher fieldset .form-group  input#inputTeacherFullName').val(),
            'age': $('#addTeacher fieldset .form-group  input#inputTeacherAge').val(),
            'address': $('#addTeacher fieldset .form-group  input#inputTeacherAddress').val(),
            'gender': $('#addTeacher fieldset .form-group  input#inputTeacherGender').val(),
            'email': $('#addTeacher fieldset .form-group  input#inputTeacherEmail').val(),
            'major': $('#addTeacher fieldset .form-group  input#inputTeacherMajor').val()
        }
        $.ajax({
            type: 'POST',
            data: newTeacher,
            url: '/teachers/addteacher',
            dataType: 'JSON'
        }).done(function( response ) {
            if (response.msg === '') {
                $('#addTeacher fieldset .form-group input').val('');
                initTableTeacher();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
};

function updateTeacher(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#updateTeacher input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });
    if(errorCount === 0) {
        var modifiedTeacher = {
            'tc_id': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherID').val(),
            'fullname': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherFullName').val(),
            'age': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherAge').val(),
            'address': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherAddress').val(),
            'gender': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherGender').val(),
            'email': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherEmail').val(),
            'major': $('#updateTeacher fieldset .form-group input#inputUpdateTeacherMajor').val()
        }
        $.ajax({
            type: 'PUT',
            data: modifiedTeacher,
            url: '/teachers/updateteacher/' + teacherId,
            dataType: 'JSON'
        }).done(function( response ) {
            if (response.msg === '') {
                $('#updateTeacher fieldset .form-group input').val('');
                initTableTeacher();
            }
            else {
                //alert('Error: ' + response.msg);
                console.log(response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
};
function deleteTeacher(event) {
    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/teachers/deleteteacher/' + $(this).attr('rel')
        }).done(function( response ) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            initTableTeacher();
        });
    }
    else {
        return false;
    }
};
function initTableStudent() {
    var tableContent = '';
    $.getJSON( '/students/studentlist', function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.st_id + '</td>';
            tableContent += '<td><a href="#" class="linkshowstudent" rel="' + this.fullname + '" title="Show Details">' + this.fullname + '</a></td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td><a class="btn btn-danger linkdeletestudent" rel="' + this._id + '">Delete</a></td>';
            tableContent += '</tr>';
            studentListData = data;                                   
        });
        $('#studentList table tbody').html(tableContent);
    });
};
function showStudentInfo(event) {
    event.preventDefault();
    var thisStudentName = $(this).attr('rel');
    var arrayPosition = studentListData.map(function(arrayItem) { return arrayItem.fullname; }).indexOf(thisStudentName);
    var thisStudentObject = studentListData[arrayPosition];
    $('#studentInfoName').text(thisStudentObject.fullname);
    $('#studentInfoAge').text(thisStudentObject.age);
    $('#studentInfoGender').text(thisStudentObject.gender);
    $('#studentInfoLocation').text(thisStudentObject.address);
    studentId = thisStudentObject._id;
    $('#updateStudent fieldset .form-group input#inputUpdateStudentID').val(thisStudentObject.st_id);
    $('#updateStudent fieldset .form-group input#inputUpdateStudentFullName').val(thisStudentObject.fullname);
    $('#updateStudent fieldset .form-group input#inputUpdateStudentAge').val(thisStudentObject.age);
    $('#updateStudent fieldset .form-group input#inputUpdateStudentAddress').val(thisStudentObject.address);
    $('#updateStudent fieldset .form-group input#inputUpdateStudentGender').val(thisStudentObject.gender);
    $('#updateStudent fieldset .form-group input#inputUpdateStudentEmail').val(thisStudentObject.email);
};
function addStudent(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#addStudent input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });
    if(errorCount === 0) {
       
        var newStudent = {
            'st_id': $('#addStudent fieldset .form-group input#inputStudentID').val(),
            'fullname': $('#addStudent fieldset .form-group  input#inputStudentFullName').val(),
            'age': $('#addStudent fieldset .form-group  input#inputStudentAge').val(),
            'address': $('#addStudent fieldset .form-group  input#inputStudentAddress').val(),
            'gender': $('#addStudent fieldset .form-group  input#inputStudentGender').val(),
            'email': $('#addStudent fieldset .form-group  input#inputStudentEmail').val(),
            'major': $('#addStudent fieldset .form-group  input#inputStudentMajor').val()
        }
        
        $.ajax({
            type: 'POST',
            data: newStudent,
            url: '/students/addstudent',
            dataType: 'JSON'
        }).done(function( response ) {

            if (response.msg === '') {
  
                $('#addStudent fieldset .form-group input').val('');
                initTableStudent();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
};
function updateStudent(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#updateStudent input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });
    if(errorCount === 0) {
        var modifiedStudent = {
            'st_id': $('#updateStudent fieldset .form-group input#inputUpdateStudentID').val(),
            'fullname': $('#updateStudent fieldset .form-group input#inputUpdateStudentFullName').val(),
            'age': $('#updateStudent fieldset .form-group input#inputUpdateStudentAge').val(),
            'address': $('#updateStudent fieldset .form-group input#inputUpdateStudentAddress').val(),
            'gender': $('#updateStudent fieldset .form-group input#inputUpdateStudentGender').val(),
            'email': $('#updateStudent fieldset .form-group input#inputUpdateStudentEmail').val(),
        }
        $.ajax({
            type: 'PUT',
            data: modifiedStudent,
            url: '/students/updatestudent/' + studentId,
            dataType: 'JSON'
        }).done(function( response ) {
            if (response.msg === '') {
                $('#updateStudent fieldset .form-group input').val('');
                initTableStudent();
            }
            else {
                //alert('Error: ' + response.msg);
                console.log(response.msg);
            }
        });
    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};
function deleteStudent(event) {
    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/students/deletestudent/' + $(this).attr('rel')
        }).done(function( response ) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            initTableStudent();
        });
    }
    else {
        return false;
    }
};
function initTableRegistration() {
    var tableContent = '';
    $.getJSON( '/registration/registrationlist', function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.cl_id + '</td>';
            tableContent += '<td>' + this.classname + '</td>';
            tableContent += '<td>' + this.tcname + '</td>';
            tableContent += '<td><a href="#" class="linkshowdetail" rel="' + this.cl_id + '" type="button" data-toggle="modal" data-target="#showDetailModal">Show Details</a></td>';
            tableContent += '<td><a class="btn btn-danger linkremoveclass" rel="' + this._id + '">Remove</a></td>';
            tableContent += '</tr>';
            classListData = data;                                   
        });
        $('#registrationList table tbody').html(tableContent);
    });
};
function addClass(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#addClass input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });
    var date = new Date($('#addClass fieldset .form-group input#inputDate').val());
    if(errorCount === 0) {
        var newClass = {
            'cl_id': $('#addClass fieldset .form-group input#inputClassID').val(),
            'classname': $('#addClass fieldset .form-group input#inputClassName').val(),
            'tcname': $('#addClass fieldset .form-group input#inputTeacher').val(),
            'day': date.getDate(),
            'month': date.getMonth() + 1,
            'year': date.getFullYear()
        }
        $.ajax({
            type: 'POST',
            data: newClass,
            url: '/registration/addclass',
            dataType: 'JSON'
        }).done(function( response ) {
            if (response.msg === '') {
                $('#addClass fieldset .form-group input').val('');
                initTableRegistration();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
};
function removeClass(event) {
    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/registration/removeclass/'+  $(this).attr('rel')
        }).done(function( response ) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            initTableRegistration();
        });
    }
    else {
        return false;
    }
};
function initTableStudentList() {
    var tableContent = '';
    currentClassID = $(this).attr('rel');
    $.getJSON( '/registration/registrationlist/' + currentClassID, function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.st_id + '</td>';
            tableContent += '<td>' + this.st_name + '</td>';
            tableContent += '<td><a class="btn btn-danger linkremovestudent" rel="' + this._id + '">Remove</a></td>';            
            tableContent += '</tr>';
            //classListData = data;                                   
        });
        $('#classList table tbody').html(tableContent);
    });
};
function refreshTableStudentList() {
    var tableContent = '';
    $.getJSON( '/registration/registrationlist/' + currentClassID, function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.st_id + '</td>';
            tableContent += '<td>' + this.st_name + '</td>';
            tableContent += '<td><a class="btn btn-danger linkremovestudent" rel="' + this._id + '">Remove</a></td>';            
            tableContent += '</tr>';
            //classListData = data;                                   
        });
        $('#classList table tbody').html(tableContent);
    });
};
function addStudentToClass(event) {
    event.preventDefault();
    var errorCount = 0;
    $('#addStudentToClass input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });
    if(errorCount === 0) {
        var newStudent = {
            'cl_id': currentClassID,
            'st_id': $('#addStudentToClass fieldset .form-group input#inputStudentIDToClass').val(),
            'st_name': $('#addStudentToClass fieldset .form-group input#inputStudentFullNameToClass').val()
        }
        $.ajax({
            type: 'POST',
            data: newStudent,
            url: '/registration/addstudenttoclass',
            dataType: 'JSON'
        }).done(function( response ) {
            if (response.msg === '') {
                $('#addStudentToClass fieldset .form-group input').val('');
                refreshTableStudentList();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
};
function removeStudentFromClass(event) {
    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/registration/removestudentfromclass/'+  $(this).attr('rel')
        }).done(function( response ) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            refreshTableStudentList();
        });
    }
    else {
        return false;
    }
};
var events = [];
var settings = {};
var element = document.getElementById('caleandar');
function putClassIntoCalendar() {
    $.getJSON( '/registration/registrationlist', function( data ) {
        $.each(data, function(){
            //console.log(this.year);
          events.push({'Date': new Date(this.year, this.month - 1, this.day), 'Title': this.classname});                  
        });
        classListData = data;
        caleandar(element, events, settings);
    }); 
};
function showClassDetail(event)
{
    event.preventDefault();
    var thisClassName = $(this).attr('rel');
    var arrayPosition = classListData.map(function(arrayItem) { return arrayItem.classname; }).indexOf(thisClassName);
    var thisClassObject = classListData[arrayPosition];
    $('#classInfoID').text(thisClassObject.cl_id);
    $('#classInfoName').text(thisClassObject.classname);
    $('#classInfoTeacher').text(thisClassObject.tcname);
    $('#classInfoDate').text(thisClassObject.day + '/' + thisClassObject.month + '/' + thisClassObject.year );
}